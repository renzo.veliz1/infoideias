<?php

class tools{
    //calcular seculos do ano
    public function SeculoAno($ano=1905){
        $seculo=0;
        try {
            if(is_numeric($ano)&&!is_float($ano)){
                $seculo= ($ano/100);
                if(is_float($seculo)){
                    $seculo=(int)($seculo +1);
                }else{
                    $seculo=(int)$seculo;
                }
            }else{
                throw new Exception("Numero nao sao validos");
            }          
        } catch (Exception $th) {
            echo "Error datos nao sao validos";
            exit();
        }

        echo "Ano $ano = século : $seculo";
    }

    public function Primos($inicial=10, $final=29){
        $arrayPrimos=[];
        for ($i=$inicial+1; $i < $final; $i++) { 
            $isPrimo=true;
            for ($j=2; $j < $i ; $j++) {
                if($i%$j == 0){
                    $isPrimo=false;
                    break;
                }
            }
            if($isPrimo){
                array_push($arrayPrimos, $i);
            }
        }
        echo '['.implode(",", $arrayPrimos).']';
    }

    public function NumerosNaoRepetidos(){
        //carregamos o array com numeros aleatorios
        $arrayNumAleatorios=[];
        for ($i=0; $i < 20 ; $i++) { 
            array_push($arrayNumAleatorios, rand(1,10));
        }

        $numNaoRepetidos=[];
        for ($i=0; $i < count($arrayNumAleatorios); $i++) {
            $procurar=$arrayNumAleatorios[$i];
            $quantidadeVeces=count(array_keys($arrayNumAleatorios, $procurar));
            if ($quantidadeVeces==1) {
                array_push($numNaoRepetidos, $procurar);
            }
        }

        echo "Array sorteado = [".implode(",",$arrayNumAleatorios)."] <br>";
        echo "Os números que não se repetem são = [".implode(",",$numNaoRepetidos)."]";

    }


    public function SequenciaCrescente($array=[]){
        $simPode=false;
        if (count($array)>1) {
            for ($i=0; $i < count($array); $i++) { 
                $arrayClone=$array;
                if(!$simPode){
                    unset($arrayClone[$i]);
                    $simPode=true;
                    for($j=0; $j<count($arrayClone) ;$j++){
                        if(count($arrayClone)>1&&$j!=(count($arrayClone)-1)){
                            if($arrayClone[$j]>=$arrayClone[$j+1]){
                                $simPode=false;
                                break;
                            }
                        }                     
                    }
                }else{
                    break;
                }
            }
        }

        echo "[".implode(",",$array)."]  ".($simPode?"true":"false")."<br>";
    }


}

$tools= new tools;

//funtion seculo ano
//$tools->SeculoAno(1905);

//funcao de numeros primos
//$tools->Primos(10,29);

//fucnao de numeros nao repetidos
//$tools->NumerosNaoRepetidos();


//fucnao de secuencia incrementavel
$tools->SequenciaCrescente([1, 3, 2, 1]); //false
$tools->SequenciaCrescente([1, 3, 2]); //true
$tools->SequenciaCrescente([1, 2, 1, 2]); //false
$tools->SequenciaCrescente([3, 6, 5, 8, 10, 20, 15]); //false
$tools->SequenciaCrescente([1, 1, 2, 3, 4, 4]); //false
$tools->SequenciaCrescente([1, 4, 10, 4, 2]); //false
$tools->SequenciaCrescente([10, 1, 2, 3, 4, 5]); //true
$tools->SequenciaCrescente([1, 1, 1, 2, 3]); //false
$tools->SequenciaCrescente([0, -2, 5, 6]); //true
$tools->SequenciaCrescente([1, 2, 3, 4, 5, 3, 5, 6]); //false
$tools->SequenciaCrescente([40, 50, 60, 10, 20, 30]); //false
$tools->SequenciaCrescente([1, 1]); //true
$tools->SequenciaCrescente([1, 2, 5, 3, 5]); //true
$tools->SequenciaCrescente([1, 2, 5, 5, 5]); //false
$tools->SequenciaCrescente([10, 1, 2, 3, 4, 5, 6, 1]); //false
$tools->SequenciaCrescente([1, 2, 3, 4, 3, 6]); //true
$tools->SequenciaCrescente([1, 2, 3, 4, 99, 5, 6]); //true
$tools->SequenciaCrescente([123, -17, -5, 1, 2, 3, 12, 43, 45]); //true
$tools->SequenciaCrescente([3, 5, 67, 98, 3]); //true
